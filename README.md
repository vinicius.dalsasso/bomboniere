# Bomboniere

Projeto da prova prática da sprint 3 parte 2

## Pipoca e Bebida

Recebe informações do Pipocas e Bebida.
Utilizam herança da classe abstrata produto.

## Enum de Tamanhos
Contem enum com a definição de nomes de tamanhos assim como a quantidade em gramas de cada tamanho.

## Encher balde

Função que emite mensagem com o tipo da pipoca, caso exista uma pipoca inserida.

## Verificações de Erros
Verificações de erro via try catch geral, bem como na inserção de caracteres não numéricos em campos int/double.
