package br.com.proway.bomboniere.model;

public class Bebida extends Produto{
    private int ml;
    private String marca;

    public Bebida(int id, String nome, double preco, Fornecedor fornecedor, int ml, String marca) {
        super(id, nome, preco, fornecedor);
        this.ml = ml;
        this.marca = marca;
    }

    public int getMl() {
        return ml;
    }

    public void setMl(int ml) {
        this.ml = ml;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }
}
