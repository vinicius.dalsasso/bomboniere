package br.com.proway.bomboniere.model;

import br.com.proway.bomboniere.utils.Tamanho;

public class Pipoca extends Produto implements Acao{

    private char tipo;
    private Tamanho tamanho;

    public Pipoca(int id, String nome, double preco, Fornecedor fornecedor, char tipo, Tamanho tamanho) {
        super(id, nome, preco, fornecedor);
        this.tipo = tipo;
        this.tamanho = tamanho;
    }

    public char getTipo() {
        return tipo;
    }

    public void setTipo(char tipo) {
        this.tipo = tipo;
    }

    public Tamanho getTamanho() {
        return tamanho;
    }

    public void setTamanho(Tamanho tamanho) {
        this.tamanho = tamanho;
    }

    @Override
    public String encherBalde(Pipoca pipoca) {
        String retorno = "";
        if(pipoca.getNome() == "") {
            retorno = "Informe uma pipoca antes de tentar encher o balde!";
        } else {
            String tipo;
            if(pipoca.getTipo() == 'S') {
                tipo = "salgada";
            } else {
                tipo = "doce";
            }
            retorno = "Foi enchido o balde com pipoca " + tipo + ".";
        }

        return retorno;
    }
}
