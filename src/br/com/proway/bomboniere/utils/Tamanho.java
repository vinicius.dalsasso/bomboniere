package br.com.proway.bomboniere.utils;

public enum Tamanho {
    PEQUENA(1, "Pequena", 100),
    MEDIA(2, "Média", 200),
    GRANDE(3, "Grande", 300);

    private final int id;
    private final String nome;
    private final int grama;

    Tamanho(int id, String nome, int grama) {
        this.id = id;
        this.nome = nome;
        this.grama = grama;
    }

    public int getId() {
        return id;
    }

    public String getNome() {
        return nome;
    }

    public int getGrama() {
        return grama;
    }
}

