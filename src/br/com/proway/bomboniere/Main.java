package br.com.proway.bomboniere;

import br.com.proway.bomboniere.model.Bebida;
import br.com.proway.bomboniere.model.Fornecedor;
import br.com.proway.bomboniere.model.Pipoca;
import br.com.proway.bomboniere.utils.Tamanho;

import javax.swing.*;
import java.text.DecimalFormat;
import java.util.Arrays;

public class Main {

    // usado para formatar dinheiro
    private static final DecimalFormat df = new DecimalFormat("0.00");

    public static void main (String[] args) {

        try {
            // Criacoes de fornecedor manual
            Fornecedor fornecedor1 = new Fornecedor(1, "", "1234");

            // Criacao de objetos manual
            Bebida bebida1 = new Bebida(1, "", 0, fornecedor1, 0, " ");
            Pipoca pipoca1 = new Pipoca(1, "", 0, fornecedor1, ' ', Tamanho.GRANDE);

            char tamanhoPipoca = ' ';

            while (true) {
                String texto = "                                                                              Sistema de Bomboniere\n";

                String[] opcoes = {"Cadastrar Fornecedor","Cadastrar Pipoca", "Cadastrar Bebida", "Encher balde", "Sair"};

                int opcao = JOptionPane.showOptionDialog(null, texto, "", JOptionPane.DEFAULT_OPTION, JOptionPane.INFORMATION_MESSAGE, null, opcoes, opcoes[0]);

                if(opcao == 0) {
                    fornecedor1.setNome(JOptionPane.showInputDialog(null, "Informe o nome do fornecedor"));
                    fornecedor1.setCnpj(JOptionPane.showInputDialog(null, "Informe o CNPJ do fornecedor"));

                    JOptionPane.showMessageDialog(null, "Inserido fornecedor " + fornecedor1.getNome() + ", com o CNPJ " + fornecedor1.getCnpj() + ".");
                } else if (opcao == 1) {

                    if(fornecedor1.getNome() != "") {
                        pipoca1.setNome(JOptionPane.showInputDialog("Informe o nome desta opção de pipoca"));
                        while (pipoca1.getPreco() <= 0) {
                            try {
                                pipoca1.setPreco(Double.parseDouble(JOptionPane.showInputDialog("Informe o preço")));
                            } catch (Exception e) {
                                JOptionPane.showInputDialog(null, "Vocẽ deve informar um valor numérico e maior que  0 para o preço!");
                            }
                        }

                        // continua pedindo o tipo até que o usuário informe S ou D
                        while(pipoca1.getTipo()!= 'S' && pipoca1.getTipo()!= 'D') {
                            pipoca1.setTipo(JOptionPane.showInputDialog("Informe o tipo ('S' para Salgada/'D' para Doce)").toUpperCase().charAt(0));
                        }

                        String tipoPipoca;
                        // utilizado para o print do tipo
                        if (pipoca1.getTipo() == 'S') {
                            tipoPipoca = "Salgada";
                        } else {
                            tipoPipoca = "Doce";
                        }

                        Tamanho listaTamanho = Arrays.stream(Tamanho.values()).filter(d -> d.getId() == 2).findFirst().get();
                        Tamanho tamanho = (Tamanho) JOptionPane.showInputDialog(null, "Escolha o tamanho da pipoca", "Menu", JOptionPane.PLAIN_MESSAGE, null, listaTamanho.values(), listaTamanho.values()[0]);
                        pipoca1.setTamanho(tamanho);

                        JOptionPane.showMessageDialog(null, "Foi cadastrada uma pipoca " + pipoca1.getTamanho().getNome() + " de " + pipoca1.getTamanho().getGrama() + " gramas, com o nome " + pipoca1.getNome() + ", que custa R$ " + df.format(pipoca1.getPreco()) + ", do tipo " + tipoPipoca + ".\nFornecedor: " + fornecedor1.getNome() + ". CNPJ: " + fornecedor1.getCnpj() + ".");
                    } else {
                        JOptionPane.showMessageDialog(null, "Favor inserir um fornecedor antes de tentar cadastrar uma pipoca.");
                    }
                } else if (opcao == 2) {

                    if(fornecedor1.getNome() != "") {
                        bebida1.setNome(JOptionPane.showInputDialog("Informe o nome da Bebida"));

                        while(bebida1.getPreco() <= 0) {
                            try {
                                bebida1.setPreco(Double.parseDouble(JOptionPane.showInputDialog("Informe a quantidade de ML da bebida")));
                            } catch (Exception e) {
                                JOptionPane.showMessageDialog(null, "É necessário informar um valor numérico maior que 0 para a quantidade de ml");
                            }
                        }

                        bebida1.setMarca(JOptionPane.showInputDialog("Informe a marca da bebida"));

                        JOptionPane.showMessageDialog(null, "Foi cadastrada uma bebida com o nome " + bebida1.getNome() + ", com o preço de R$ " + df.format(bebida1.getPreco()) + ", que contem " + bebida1.getMl() + "ml.\nFornecedor: " + fornecedor1.getNome() + ".CNPJ: " + fornecedor1.getCnpj());
                    } else {
                        JOptionPane.showMessageDialog(null, "Favor inserir um fornecedor antes de cadastrar uma bebida");
                    }

                } else if (opcao == 3) {
                    String mensagem = pipoca1.encherBalde(pipoca1);
                    JOptionPane.showMessageDialog(null, mensagem);
                } else {
                    break;
                }
            }

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Oops... algo deu errado.");
        }
    }
}
